#ifndef GAME_H
#define GAME_H

#include <QList>
#include <QMainWindow>
#include "../Interface/mainfenetre.h"
#include "Personnage/Joueur/joueur.h"

class Game
{
    Q_OBJECT

public:
    Game();
    void launch_game();
signals:

public slots:
    void set_nbjoueur(int);

private:
    MainFenetre* mainWindow;
    int nbjoueur;
    QList<Joueur*>* l_joueur;

};

#endif // GAME_H
