#include "game.h"

Game::Game()
{
    MainFenetre* window = new MainFenetre();
    mainWindow = window;
}

void Game::launch_game()
{
    QObject::connect(mainWindow, SIGNAL(set_nb(int)), this, SLOT(set_nbjoueur(int)));
    mainWindow->launch();
}

void Game::set_nbjoueur(int nbjoueur)
{
    this->nbjoueur = nbjoueur;
}
