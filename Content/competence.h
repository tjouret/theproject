#ifndef COMPETENCE_H
#define COMPETENCE_H

enum Comp {
    Ambidextrie,
    Agoraphobie,
    AppelDesRenforts,
    AppelDuSauvage,
    AppelDuTonneau,
    AppelDuVentre,
    ArmesDeBourrin,
    ArnaqueCarambouille,
    AttireMonstres,
    BourrePif,
    BricoloDuDimanche,
    ChanceDuRempailleur,
    ChefDeGroupe,
    ChercherDesNoises,
    Chevaucher,
    Chouraver,
    ComprendreLesAnimaux,
    Cuistot,
    DeplacementSilencieux,
    Desamorcer,
    Detecter,
    Erudition,
    Escalader,
    Forgeron,
    FrapperLachement,
    Fariboles,
    FouillesDansLesPoubelles,
    InstinctDeSurvie,
    InstinctDuTresor,
    Intimider,
    JonglageEtDanse,
    LangueDesMonstres,
    Mefiance,
    MendierEtPleurnicher,
    Nager,
    NaiveteTouchante,
    Penible,
    Pister,
    PremiersSoins,
    Radin,
    Recuperation,
    RessembleARien,
    RunesBizarres,
    SentirDesPieds,
    Serrurier,
    TeteVide,
    TirerCorrectement,
    TomberDansLesPieges,
    TrucDeMauviette
};

typedef Comp e_comp;

class Competence
{
public:
    Competence();

    int get_id();
private:
    int id;
    e_comp comp;
};

#endif // COMPETENCE_H
