#ifndef JOUEUR_H
#define JOUEUR_H

#include "../personnage.h"
#include "../../../Utility/rand.h"

enum Origine {
    Humain,
    Barbare,
    Nain,
    HautElfe,
    DemiElfe,
    ElfeSylvain,
    ElfeNoir,
    Orque,
    DemiOrque,
    Gobelin,
    Ogre,
    SemiHomme,
    GnomeForetNord
};

enum Metier {
    Guerrier,
    Ninja,
    Voleur,
    Pretre,
    Mage,
    Paladin,
    Ranger,
    Menestrel,
    Pirate,
    Marchand,
    Ingenieur,
    Bourgeois
};

typedef Origine e_origine;
typedef Metier e_metier;

class Joueur : public Personnage
{
public:
    Joueur();
private:
    e_origine origine;
    e_metier metier;
    int charisme;
    int poid_max;
    int exp;
    int pd; // points de destin
};

#endif // JOUEUR_H
