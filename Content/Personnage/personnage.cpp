#include "personnage.h"

Personnage::Personnage()
{
    Rand* rand = new Rand();

    this->courage = rand->randInt(8, 13);
    this->intelligence = rand->randInt(8, 13);
    this->adresse = rand->randInt(8, 13);
    this->force = rand->randInt(8, 13);
    this->attaque = 8;
    this->parade = 10;
    this->pv = 0;
    this->pa = 0;
    this->protection = 0;
    this->argent = rand->randInt(20, 120);
    this->res_magie = (this->force + this->intelligence + this->courage) / 3;
    this->is_magic = true;

    //caracteristique superieures/inferieures
    this->impact = 0;
    if (this->force == 13)
        this->impact++;
    if (this->force == 8)
        this->impact--;
    this->degat_sorts = 0;
    if (this->intelligence == 13)
        this->degat_sorts++;

    this->comp = NULL;
    this->obj = NULL;
    this->arm = NULL;
}

void Personnage::add_arm(Armure* armure)
{
    this->arm->append(armure);
}

void Personnage::add_obj(Objet* objet)
{
    this->obj->append(objet);
}

void Personnage::add_comp(Competence* competence)
{
    this->comp->append(competence);
}

void Personnage::remove_arm(int id)
{
    QList<Armure*>* armureList = this->arm;
    Armure* current;

    for (int i = 0; i < armureList->size(); i++)
    {
        current = armureList->takeAt(i);
        if (current->get_id() == id)
            this->arm->removeAt(i);
    }
}

void Personnage::remove_obj(int id)
{
    QList<Objet*>* objList = this->obj;
    Objet* current;

    for (int i = 0; i < objList->size(); i++)
    {
        current = objList->takeAt(i);
        if (current->get_id() == id)
            this->obj->removeAt(i);
    }
}

void Personnage::remove_comp(int id)
{
    QList<Competence*>* compList = this->comp;
    Competence* current;

    for (int i = 0; i < compList->size(); i++)
    {
        current = compList->takeAt(i);
        if (current->get_id() == id)
            this->comp->removeAt(i);
    }
}

void Personnage::set_courage(int courage)
{
    this->courage = courage;
}

void Personnage::set_intelligence(int intelligence)
{
    this->intelligence = intelligence;
}

void Personnage::set_adresse(int adresse)
{
    this->adresse = adresse;
}

void Personnage::set_force(int force)
{
    this->force = force;
}

void Personnage::set_attaque(int attaque)
{
    this->attaque = attaque;
}

void Personnage::set_parade(int parade)
{
    this->parade = parade;
}

void Personnage::set_res_magie(int res_magie)
{
    this->res_magie = res_magie;
}

void Personnage::set_protection(int protection)
{
    this->protection = protection;
}

void Personnage::set_impact(int impact)
{
    this->impact = impact;
}

void Personnage::set_degat_sort(int degat_sort)
{
    this->degat_sorts = degat_sort;
}

void Personnage::set_pv(int pv)
{
    this->pv = pv;
}

void Personnage::set_pa(int pa)
{
    this->pa = pa;
}

void Personnage::set_argent(int argent)
{
    this->argent = argent;
}

int Personnage::get_courage()
{
    return this->courage;
}

int Personnage::get_intelligence()
{
    return this->intelligence;
}

int Personnage::get_adresse()
{
    return this->adresse;
}

int Personnage::get_force()
{
    return this->force;
}

int Personnage::get_attaque()
{
    return this->attaque;
}

int Personnage::get_parade()
{
    return this->parade;
}

int Personnage::get_res_magie()
{
    return this->res_magie;
}

int Personnage::get_protection()
{
    return this->protection;
}

int Personnage::get_impact()
{
    return this->impact;
}

int Personnage::get_degat_sort()
{
    return this->degat_sorts;
}

int Personnage::get_pv()
{
    return this->pv;
}

int Personnage::get_pa()
{
    return this->pa;
}

int Personnage::get_argent()
{
    return this->argent;
}
