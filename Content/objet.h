#ifndef OBJET_H
#define OBJET_H

class Objet
{
public:
    Objet();
    int get_id();

private:
    int id;
};

#endif // OBJET_H
