#include "rand.h"

Rand::Rand()
{
    QTime time = QTime::currentTime();
    qsrand((uint)time.msec());
}

int Rand::randInt(int low, int high)
{
    return qrand() % ((high + 1) - low) + low;
}
