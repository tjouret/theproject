#ifndef RAND_H
#define RAND_H

#include <qglobal.h>
#include <QTime>

class Rand
{
public:
    Rand();
    int randInt(int low, int high);
};

#endif // RAND_H
