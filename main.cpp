#include <QApplication>
#include <QMainWindow>
#include <QStackedLayout>
#include "Content/game.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    Game* game = new Game();

    game->launch_game();

    return app.exec();
}
