SOURCES += \
    main.cpp \
    Interface/formnbjoueur.cpp \
    Interface/mainfenetre.cpp \
    Interface/testfen.cpp \
    Interface/menu.cpp \
    Interface/createchar.cpp \
    Content/objet.cpp \
    Content/armure.cpp \
    Content/competence.cpp \
    Utility/rand.cpp \
    Content/Personnage/personnage.cpp \
    Content/Personnage/monstre.cpp \
    Content/Personnage/Joueur/joueur.cpp \
    Content/Personnage/Joueur/humain.cpp \
    Content/Personnage/Joueur/barbare.cpp \
    Content/game.cpp

HEADERS += \
    Interface/formnbjoueur.h \
    Interface/mainfenetre.h \
    Interface/testfen.h \
    Interface/menu.h \
    Interface/createchar.h \
    Content/objet.h \
    Content/armure.h \
    Content/competence.h \
    Utility/rand.h \
    Content/Personnage/personnage.h \
    Content/Personnage/monstre.h \
    Content/Personnage/Joueur/joueur.h \
    Content/Personnage/Joueur/humain.h \
    Content/Personnage/Joueur/barbare.h \
    Content/game.h
