#include "mainfenetre.h"

MainFenetre::MainFenetre(QWidget *parent) :
    QMainWindow(parent)
{
    QStackedLayout *stackedLayout = new QStackedLayout(this);
    Menu* menu = new Menu(this);
    FormNbJoueur* menu_nbJoueur = new FormNbJoueur(this);
    CreateChar* menu_createChar = new CreateChar(this);

    QObject::connect(menu, SIGNAL(next()), this, SLOT(next()));
    QObject::connect(menu_nbJoueur, SIGNAL(next(int)), this, SLOT(next_nbjoueur(int)));

    stackedLayout->addWidget(menu);
    stackedLayout->addWidget(menu_nbJoueur);
    stackedLayout->addWidget(menu_createChar);

    stackedLayout->setCurrentWidget(menu);

    this->stackedLayout = stackedLayout;
}

void MainFenetre::launch()
{
    setCentralWidget(stackedLayout->currentWidget());
    show();
}

void MainFenetre::next()
{
    int index = stackedLayout->currentIndex();
    if (index == (stackedLayout->count() - 1))
        index = -1;

    stackedLayout->setCurrentWidget(stackedLayout->widget(index + 1));
    setCentralWidget(stackedLayout->currentWidget());
    show();
}

void MainFenetre::next_nbjoueur(int nbjoueur)
{
    this->nbjoueur = nbjoueur;
    emit set_nb(nbjoueur);

    stackedLayout->setCurrentWidget(stackedLayout->widget(2));
    setCentralWidget(stackedLayout->currentWidget());
    show();
}
