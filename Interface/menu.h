#ifndef MENU_H_
#define MENU_H_

#include <QApplication>
#include <QPushButton>
#include <QWidget>
#include <QFormLayout>
#include <QStackedLayout>
#include <QMainWindow>

class Menu : public QWidget
{
    Q_OBJECT

    public:
        Menu(QMainWindow* mainWindow);

    public slots:
        void lancer_jeu();

    signals:
        void next();

    private:
        QBoxLayout* set_layout();
};

#endif /* MENU_H_ */
