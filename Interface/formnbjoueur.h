#ifndef FORMNBJOUEUR_H_
#define FORMNBJOUEUR_H_

#include <QApplication>
#include <QPushButton>
#include <QWidget>
#include <QFormLayout>
#include <QStackedLayout>
#include <QMainWindow>
#include <QLabel>
#include <QLineEdit>
#include <QRegExp>
#include <QRegExpValidator>
#include "testfen.h"

class FormNbJoueur : public QWidget
{
    Q_OBJECT

    public:
        FormNbJoueur(QMainWindow* mainWindow);

    public slots:
        //void plus();
        //void moins();
        void get();
        void validate();
        int getNbJoueur();

    signals:
        void next(int);

    private:
        QLineEdit* lineEdit;
        int nbjoueur;
        QLabel* label;

        QBoxLayout* set_layout();
    
};

#endif /* FORMNBJOUEUR_H_ */
