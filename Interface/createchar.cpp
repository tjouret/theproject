#include "createchar.h"

CreateChar::CreateChar(QMainWindow* mainWindow)
{
    QGridLayout* layout = set_layout();

    setParent(mainWindow);
    setLayout(layout);
}

QGridLayout* CreateChar::set_layout()
{
    QLabel* l_force = new QLabel("Force: ");
    QLabel* l_adresse = new QLabel("Adresse: ");
    QLabel* l_courage = new QLabel("Courage: ");
    QLabel* l_intelligence = new QLabel("Intelligence: ");
    QLabel* l_parade = new QLabel("Parade: ");
    QLabel* l_argent = new QLabel("Argent: ");
    QGridLayout* grid = new QGridLayout(this);
    QComboBox* race = new QComboBox(this);
    QComboBox* metier = new QComboBox(this);
    QComboBox* competence = new QComboBox(this);
    QPushButton* generate = new QPushButton("Lancer les des", this);

    QObject::connect(generate, SIGNAL(clicked()), this, SLOT(generate_stats()));

    race->addItem("Humain");
    race->addItem("Barbare");
    race->addItem("Nain");
    race->addItem("Haut Elfe");
    race->addItem("Demi-Elfe");
    race->addItem("Elfe Sylvain");
    race->addItem("Elfe Noir");
    race->addItem("Orque");
    race->addItem("Demi-Orque");
    race->addItem("Gobelin");
    race->addItem("Ogre");
    race->addItem("Semi-Homme");
    race->addItem("Gnome des Forets du Nord");

    metier->addItem("Guerrier, Gladiateur");
    metier->addItem("Ninja, Assassin");
    metier->addItem("Voleur");
    metier->addItem("Pretre");
    metier->addItem("Mage, Sorcier");
    metier->addItem("Paladin");
    metier->addItem("Ranger");
    metier->addItem("Menestrel");
    metier->addItem("Pirate");
    metier->addItem("Marchand");
    metier->addItem("Ingenieur");
    metier->addItem("Bourgeois, Noble");

    competence->addItem("Ambidextrie");
    competence->addItem("Agoraphobie");
    competence->addItem("Appel des renforts");
    competence->addItem("Appel du sauvage");
    competence->addItem("Appel du tonneau");
    competence->addItem("Appel du ventre");
    competence->addItem("Armes de bourrin");
    competence->addItem("Arnaque et carambouille");
    competence->addItem("Attire les monstres");
    competence->addItem("Bourre-pif");
    competence->addItem("Bricolo du dimanche");
    competence->addItem("Chance du rempailleur");
    competence->addItem("Chef de groupe");
    competence->addItem("Chercher des noises");
    competence->addItem("Chevaucher");
    competence->addItem("Chouraver");
    competence->addItem("Comprendre les animaux");
    competence->addItem("Cuistot");
    competence->addItem("Deplacement silencieux");
    competence->addItem("Desamorcer");
    competence->addItem("Detecter");
    competence->addItem("Erudition");
    competence->addItem("Escalader");
    competence->addItem("Forgeron");
    competence->addItem("Frapper lachement");
    competence->addItem("Fariboles");
    competence->addItem("Fouiller dans les poubelles");
    competence->addItem("Instinct de survie");
    competence->addItem("Instinct du tresor");
    competence->addItem("Intimider");
    competence->addItem("Jonglage et danse");
    competence->addItem("Langues des monstres");
    competence->addItem("Mefiance");
    competence->addItem("Mendier et pleurnicher");
    competence->addItem("Nager");
    competence->addItem("Naivete touchante");
    competence->addItem("Penible");
    competence->addItem("Pister");
    competence->addItem("Premiers soins");
    competence->addItem("Radin");
    competence->addItem("Recuperation");
    competence->addItem("Ressemble a rien");
    competence->addItem("Runes bizarres");
    competence->addItem("Sentir des pieds");
    competence->addItem("Serrurier");
    competence->addItem("Tete vide");
    competence->addItem("Tirer correctement");
    competence->addItem("Tomber dans les pieges");
    competence->addItem("Truc de mauviette");

    grid->addWidget(l_force, 0, 0, 1, 1);
    grid->addWidget(l_adresse, 1, 0, 1, 1);

    return grid;
}

void CreateChar::generate_stats()
{

}

QVector<int>* CreateChar::contrainte_origine(QVector<int>* value)
{
    QVector<int> col1 = new QVector<int> ;
    int cool[12] = [13; 12; 0, 0, 0, 0, 12, 12, 0, 13, 0, 0];
    QVector<int> *col2 = {0, 0, 12, 0, 11, 12, 0, 0, 9, 0, 10, 8};
    QVector<int> *col3 = {12, 11, 0, 0, 0, 0, 0, 0, 0, 0, 12, 0};
    QVector<int> *col4 = {0, 0, 0, 0, 0, 0, 0, 0, 10, 0, 0, 0};
    QVector<int> *col5 = {0, 0, 12, 11, 10, 13, 0, 0, 0, 0, 0, 13};
    QVector<int> *col6 = {0, 0, 0, 0, 0, 0, 0, 11, 0, 11, 0, 0};
    QVector<int> *col7 = {0, 0, 12, 10, 12, 0, 0, 0, 0, 0, 0, 0};
    QVector<int> *col8 = {0, 0, 0, 0, 0, 0, 10, 0, 8, 10, 0, 0};
    QVector<int> *col9 = {0, 0, 11, 0, 0, 12, 0, 0, 0, 0, 10, 10};
    QVector<int> *col10 = {0, 0, 0, 0, 0, 0, 8, 10, 10, 9, 0, 0};
    QVector<QVector<int>*> *contrainte = {col1, col2, col3, col4, col5, col6, col7, col8, col9, col10};
    QVector<int>* res = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
    bool min = true;

    for (int i = 0; i < 10; i++)
    {
        res = contOrig_line(res, value->at(i / 2), contrainte->at(i), min);
        min += 1;
    }
}

QVector<int>* CreateChar::contOrig_line(QVector<int>* res, int value, QVector<int>* col, bool min)
{
    QVector<int>::Iterator* it;


    if (min)
    {
        for (int i = 0; i < 12; i++)
        {
            if (value < col->at(i))
        }
    }
}
