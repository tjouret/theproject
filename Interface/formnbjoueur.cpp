#include "formnbjoueur.h"

FormNbJoueur::FormNbJoueur(QMainWindow* mainWindow)
{
    QBoxLayout* layout = set_layout();

    setParent(mainWindow);
    this->nbjoueur = 3;
    setLayout(layout);
}

QBoxLayout* FormNbJoueur::set_layout()
{
    QLineEdit* lineEdit = new QLineEdit(this);
    QRegExp regExp("[3-9]");
    QVBoxLayout* vbox = new QVBoxLayout;
    QLabel* message = new QLabel("Nombre de joueurs:");
    QPushButton* bouton_validate = new QPushButton("Valider");

    this->lineEdit = lineEdit;

    QObject::connect(lineEdit, SIGNAL(editingFinished()), this, SLOT(get()));
    QObject::connect(bouton_validate, SIGNAL(clicked()), this, SLOT(validate()));
    lineEdit->setValidator(new QRegExpValidator(regExp, this));

    vbox->addWidget(message);
    vbox->addWidget(lineEdit);
    vbox->addWidget(bouton_validate);

    return vbox;
}

void FormNbJoueur::get()
{
    QString res = this->lineEdit->text();
    this->nbjoueur = res.toInt();
}

void FormNbJoueur::validate()
{
    emit next(this->nbjoueur);
}

int FormNbJoueur::getNbJoueur()
{
    return nbjoueur;
}
