#ifndef MAINFENETRE_H
#define MAINFENETRE_H

#include <QMainWindow>
#include <QStackedLayout>
#include "formnbjoueur.h"
#include "menu.h"
#include "createchar.h"
#include "testfen.h"

class MainFenetre : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainFenetre(QWidget *parent = 0);
    void launch();
    
signals:
    void set_nb(int);

public slots:
    void next();
    void next_nbjoueur(int nbjoueur);

private:
    QStackedLayout* stackedLayout;
    int nbjoueur;
};

#endif // MAINFENETRE_H
