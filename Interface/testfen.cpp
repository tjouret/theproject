#include "testfen.h"

TestFen::TestFen() : QWidget()
{
    setFixedSize(230, 120);

    m_boutonDialogue = new QPushButton("Ouvrir la boîte de dialogue", this);
    m_boutonDialogue->move(40, 50);

    QObject::connect(m_boutonDialogue, SIGNAL(clicked()), this, SLOT(ouvrirDialogue()));
}

void TestFen::ouvrirNext(int index)
{

    QMessageBox::information(this, "Ca marche", "current function: MainFenetre::next()\ncurrent index: " + QString::number(index));
}

void TestFen::ouvrirNext_nbjoueur(int nbjoueur)
{
    QMessageBox::information(this, "Ca marche", "current function: MainFenetre::next_nbjoueur(FormNbJoueur* menu_nbjoueur)\nnbjoueur: " + QString::number(nbjoueur));
}

void TestFen::ouvrirValidate()
{
    QMessageBox::information(this, "Ca marche", "current function: FormNbJoueur::validate()");
}
