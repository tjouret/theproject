#ifndef CREATECHAR_H
#define CREATECHAR_H

#include <QWidget>
#include <QMainWindow>
#include <QBoxLayout>
#include <QComboBox>
#include <QLabel>
#include <QGridLayout>
#include <QPushButton>
#include <QVector>
#include <QMatrix>

class CreateChar : public QWidget
{
    Q_OBJECT
public:
    explicit CreateChar(QMainWindow* mainWindow);
    void generate_stats();
    QVector<int>* contrainte_origine(QVector<int>* value);
    QVector<int>* contOrig_line(QVector<int>* res, int value, QVector<int>* col, bool min);
    
signals:
    
public slots:

private:
    QGridLayout* set_layout();
    
};

#endif // CREATECHAR_H
