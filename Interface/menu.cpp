#include "menu.h"
#include "Interface/formnbjoueur.h"

Menu::Menu(QMainWindow* mainWindow)
{
    QBoxLayout* layout = set_layout();

    setParent(mainWindow);
    setLayout(layout);
}

QBoxLayout* Menu::set_layout()
{
    QHBoxLayout* layout = new QHBoxLayout;
    QPushButton* bouton_jouer = new QPushButton("Jouer");
    QPushButton* bouton_quitter = new QPushButton("Quitter");

    bouton_jouer->setCursor(Qt::PointingHandCursor);
    QObject::connect(bouton_jouer, SIGNAL(clicked()), this, SLOT(lancer_jeu()));
    bouton_quitter->setCursor(Qt::PointingHandCursor);
    QObject::connect(bouton_quitter, SIGNAL(clicked()), qApp, SLOT(quit()));

    layout->addWidget(bouton_jouer);
    layout->addWidget(bouton_quitter);

    return layout;
}

void Menu::lancer_jeu()
{
    emit next();
}
