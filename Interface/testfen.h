#ifndef DEF_MAFENETRE
#define DEF_MAFENETRE

#include <QApplication>
#include <QWidget>
#include <QPushButton>
#include <QMessageBox>

class TestFen : public QWidget
{
    Q_OBJECT

    public:
    TestFen();

    public slots:
    void ouvrirValidate();
    void ouvrirNext(int index);
    void ouvrirNext_nbjoueur(int nbjoueur);

    private:
    QPushButton *m_boutonDialogue;
};

#endif
